using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {
        public static void CheckPreparation() {
            const string root = "test_root";
            if (!Directory.Exists(root))
                Preparation(root);
        }
        public static void Preparation(string root) {
            Directory.CreateDirectory("test_root");
            Directory.CreateDirectory("test_root\\test_1_1");
            Directory.CreateDirectory("test_root\\test_1_2");
            Directory.CreateDirectory("test_root\\test_1_3");
            Directory.CreateDirectory("test_root\\test_1_1\\test_2");
            File.Create("test_root\\txt_1.TXT");
            File.Create("test_root\\Result_Structure.txt");
            File.Create("test_root\\test_1_1\\txt_2.TXT");
            File.Create("test_root\\test_1_1\\test_2\\txt_3.TXT");
            File.Create("test_root\\test_1_2\\word_2.DOCX");
        }

        [TestMethod]
        public void TestEntries()
        {
            CheckPreparation();
            const int expectedEntries = 9;
            string testRootPath = Directory.GetCurrentDirectory() + "\\test_root";
            string testRootDir = String.Format(@"{0}", testRootPath);
            int testRootLevel = testRootDir.Split('\\').Length;
            Queue<KeyValuePair<int, string>> testFD = new Queue<KeyValuePair<int, string>>();
            dirs_files_tree.Program.DirectorySearch(testRootDir, testRootLevel, testFD);

            Assert.AreEqual(expectedEntries, testFD.Count);
        }
        [TestMethod]
        public void TestDirectory() 
        {
            /*[0, D:\test_root\txt_1.TXT]
              [0, D:\test_root\test_1_1]
              [1, D:\test_root\test_1_1\txt_2.TXT]
              [1, D:\test_root\test_1_1\test_2]
              [2, D:\test_root\test_1_1\test_2\txt_3.TXT]
              [0, D:\test_root\test_1_2]
              [1, D:\test_root\test_1_2\word_2.DOCX]
              [0, D:\test_root\test_1_3]*/
            //test path : D:\test_root\
            CheckPreparation();
            string testRootPath = Directory.GetCurrentDirectory() + "\\test_root";
            string testRootDir = String.Format(@"{0}", testRootPath);
            int testRootLevel = testRootDir.Split('\\').Length;
            Queue<KeyValuePair<int, string>> testFD = new Queue<KeyValuePair<int, string>>();
            dirs_files_tree.Program.DirectorySearch(testRootDir, testRootLevel, testFD);

            Queue<KeyValuePair<int, string>> expectedResult = new Queue<KeyValuePair<int, string>>();
            expectedResult.Enqueue(new KeyValuePair<int, string>(0, testRootPath + "\\Result_Structure.txt"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(0, testRootPath + "\\txt_1.TXT"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(0, testRootPath + "\\test_1_1"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(1, testRootPath + "\\test_1_1\\txt_2.TXT"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(1, testRootPath + "\\test_1_1\\test_2"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(2, testRootPath + "\\test_1_1\\test_2\\txt_3.TXT"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(0, testRootPath + "\\test_1_2"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(1, testRootPath + "\\test_1_2\\word_2.DOCX"));
            expectedResult.Enqueue(new KeyValuePair<int, string>(0, testRootPath + "\\test_1_3"));

            while (expectedResult.Count != 0) 
            {
                Assert.AreEqual(expectedResult.Dequeue(), testFD.Dequeue());
            }
        }
    }
}
