﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace dirs_files_tree
{
    public class Program
    {
        /*
         Test directory:
            D:\test_root\
                         [directory]test_1_1
                                   |->[directory]test_2
                                   |            |->txt_3.txt
                                   |->[file]txt_2.txt
                         [directory]test_1_2
                                   |->[file]word_2.docx
                         [directory]test_1_3
                         [file]txt_1.txt
         */
        public static void DirectorySearch(string dir, int rootLevel, Queue<KeyValuePair<int, string>> fd)
        {
            int level = rootLevel; //level 0 - root level, 1 - first level dir, 2 - second level subdir ...
            try
            {
                #region getFiles
                foreach (string f in Directory.GetFiles(dir))
                {
                    level = f.Split('\\').Length - rootLevel - 1;
                    fd.Enqueue(new KeyValuePair<int, string>(level, Path.GetFullPath(f).ToString()));
                    if (level > 0)
                    {
                        for (int i = level * 3; i > 0; --i) { Console.Write(" "); }
                        Console.Write("|->"); 
                    }
                    Console.WriteLine("[File] " + Path.GetFileName(f));                    
                    // DEBUG -> Console.WriteLine("[File] " + Path.GetFileName(f) + " Level -> " + level);
                }
                #endregion
                #region getDirectories
                foreach (string d in Directory.GetDirectories(dir))
                {
                    level = d.Split('\\').Length - rootLevel - 1;
                    fd.Enqueue(new KeyValuePair<int, string>(level, Path.GetFullPath(d).ToString()));
                    if (level > 0)
                    {
                        for (int i = level * 3; i > 0; --i) { Console.Write(" "); }
                        Console.Write("|->");
                    }
                    Console.WriteLine("[Directory] " + Path.GetFileName(d));                   
                    // DEBUG -> Console.WriteLine("[Directory] " + Path.GetFileName(d) + " Level -> " + level);
                    DirectorySearch(d, rootLevel, fd);
                }
                #endregion
               
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void Main()
        {
            while (true)
            {
                #region getInputAndProceeed
                Console.WriteLine("Get files & directories tree. v1.0");
                Console.WriteLine();
                Console.Write("Enter the path (example : C:\\some\\path) : ");
                string rootPath = Console.ReadLine();
                string rootDir = String.Format(@"{0}", rootPath);
                int rootLevel = rootDir.Split('\\').Length;
                Queue<KeyValuePair<int, string>> fd = new Queue<KeyValuePair<int, string>>();
                Console.WriteLine();
                #endregion

                
                DirectorySearch(rootDir, rootLevel, fd);
                

                Console.WriteLine();

                /*//DEBUG
                while (fd.Count != 0) 
                {
                    Console.WriteLine(fd.Dequeue());
                }*/
                #region writeToFile
                string fileName = "Result_Structure.txt";
                string resultPath = String.Format(@"{0}\{1}", rootDir, fileName);
                string temp;
                if (!File.Exists(resultPath))
                {
                    try
                    {
                        using (StreamWriter sw = new StreamWriter(resultPath, false))
                        {
                            sw.WriteLine("Get files & directories tree. v1.0");
                            sw.WriteLine();
                            sw.WriteLine("Root directory : " + rootDir);
                            sw.WriteLine();
                            while (fd.Count > 0)
                            {
                                KeyValuePair<int, string> tempKVP = new KeyValuePair<int, string>();
                                tempKVP = fd.Peek();
                                if (tempKVP.Key > 0)
                                {
                                    for (int i = tempKVP.Key * 3; i > 0; --i) { sw.Write(" "); }
                                    sw.Write("|->");
                                }
                                temp = tempKVP.Value.ToString();

                                temp = String.Join(@"\", temp.Split('\\').Skip(rootLevel + tempKVP.Key));
                                sw.WriteLine(temp);
                                fd.Dequeue();
                            }
                        }
                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                #endregion
                #region makeItRepeat
                Console.WriteLine();
                Console.WriteLine("Want to continue? (Y/n) : ");
                string answer = Console.ReadLine().ToLower();
                bool flag = true;
                while (flag)
                {
                    if (answer == "n") { System.Environment.Exit(-1); }
                    else if (answer == "y") { Console.Clear(); flag = false; ; }
                    else { 
                        Console.WriteLine("Wrong input, write Y or n!");
                        Console.WriteLine("Want to continue? (Y/n) : ");
                        answer = Console.ReadLine().ToLower();
                    }
                }
                #endregion
            }
        }
    }
}
